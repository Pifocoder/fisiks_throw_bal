import React from "react";
import styles from './Vector.module.css'
const pixelLength = 66.0;
function Vector({positionX, positionY, vectorX, vectorY}) {

    return <div className={styles.vector} style ={{
        top: -34 - positionY * pixelLength,
        left: 19 + positionX * pixelLength,
        transform: "rotate(" + Math.atan(vectorY / vectorX) * 180 / Math.PI + "deg)"
    }}></div>
}
export default Vector;