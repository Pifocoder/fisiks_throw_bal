import React, { useState, useEffect } from "react";
import styles from "./graph.module.css";
let startPositionX = 0;
let startPositionY = 0;
const startSpeedX = 0;
const startSpeedY = 0;

const updateTime = () => {
  const date = new Date();
  return (
    date.getHours() * 60 * 60 * 1000 +
    date.getMinutes() * 60 * 1000 +
    date.getSeconds() * 1000 +
    date.getMilliseconds()
  );
};

let i = 0;
function decorateX(distance) {
  let t = Math.floor(distance / 15);
  distance -= 15 * t;
  return distance
}
function decorateY(distance) {
  let t = Math.floor(distance / 10);
  distance -= 10 * t;
  return distance
}
const pixelLength = 66.0;
const rerenderTime = 1;
function Graph({ params }) {
  const [objectPositionX, setObjectPositionX] = useState(startPositionX);
  const [objectPositionY, setObjectPositionY] = useState(startPositionY);
  const [objectSpeedX, setObjectSpeedX] = useState(startSpeedX);
  const [objectSpeedY, setObjectSpeedY] = useState(startSpeedY);
  const [objectDistance, setObjectDistance] = useState(0);
  const [objectMaxHeight, setObjectMaxHeight] = useState(0);
  const [isStoped, setIsStoped] = useState(false);
  const toggleStop = () => {
    setIsStoped(!isStoped);
  };

  React.useEffect(() => {
    if (!isStoped) {
      let lastTime = updateTime();
      let speedX = params.speed * Math.cos(params.alpha);
      let speedY = params.speed * Math.sin(params.alpha);
      const windshield = params.windshield;
      const friction = params.friction;
      let positionX = startPositionX;
      let positionY = startPositionY;
      let distance = 0;
      let maxHeight = 0;
      if (objectPositionX != 0 && objectPositionY != 0) {
        positionX = objectPositionX;
        positionY = objectPositionY;
        speedX = objectSpeedX;
        speedY = objectSpeedY;
        distance = objectDistance; 
        maxHeight = objectMaxHeight;
      }
      const updatePosition = setInterval(() => {
        if (positionY >= 0) {
          const newTime = updateTime();
          distance += ((newTime - lastTime) / 1000.0) * speedX;
          setObjectDistance(distance);
          positionX = positionX + ((newTime - lastTime) / 1000.0) * speedX;
          positionY = positionY + ((newTime - lastTime) / 1000.0) * speedY;
          maxHeight = Math.max(maxHeight, positionY);
          setObjectMaxHeight(maxHeight);
          setObjectPositionX(positionX);
          setObjectPositionY(positionY);
          const accelerationX =
            (-friction * speedX -
              windshield *
                speedX *
                Math.sqrt(speedX * speedX + speedY * speedY)) /
            params.weight;
          const accelerationY =
            (friction * speedY +
              9.8 * params.weight +
              windshield *
                speedY *
                Math.sqrt(speedX * speedX + speedY * speedY)) /
            params.weight;

          speedX = speedX + ((newTime - lastTime) / 1000.0) * accelerationX;
          speedY = speedY - ((newTime - lastTime) / 1000.0) * accelerationY;
          setObjectSpeedX(speedX);
          setObjectSpeedY(speedY);
          lastTime = newTime;
        } else {
          setObjectPositionY(0);
        }
      }, rerenderTime);
      return () => clearInterval(updatePosition);
    } else {

    }
  }, [params, isStoped]);
  return (
    <section className={styles.body}>
      <section className={styles.graph} onClick={() => toggleStop()}></section>
      <div className={styles.area}>
        <section
          className={styles.object}
          style={{
            top: -34 - decorateY(objectPositionY) * pixelLength,
            left: 19 + decorateX(objectPositionX) * pixelLength,
          }}
        ></section>
      </div>
      <section className={styles.info}>
        Дальность: {Math.round(objectDistance * 1000.0) / 1000.0}{" "}
        м
      </section>
      <section className={styles.info}>
        Высота: {Math.round(objectMaxHeight * 1000.0) / 1000.0}{" "}
        м
      </section>
    </section>
  );
}

export default Graph;
