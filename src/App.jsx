import React, { useState, useEffect } from "react";
import Graph from "./graph";
import styles from "./App.module.css";

const kInf = 1000;
const decorateValues = (value) => {
  if (value === "") {
    return 0;
  } else {
    return parseFloat(value);
  }
};
const outputDecorate = (value) => {
  return Math.round(value, 1);
};
function updateAcceleration(power, weight) {
  return power / weight;
}
function getErrors(
  alphaCalc,
  speedCalc,
  frictionCalc,
  windshieldCalc,
  weightCalc
) {
  const object = {
    height: 0,
    distance: 0,
    speedX: speedCalc * Math.cos(alphaCalc),
    speedY: speedCalc * Math.sin(alphaCalc),
    accelerationX: updateAcceleration(
      -frictionCalc * speedCalc * Math.cos(alphaCalc) -
        windshieldCalc *
          speedCalc *
          Math.cos(alphaCalc) *
          Math.sqrt(
            speedCalc * Math.cos(alphaCalc) * speedCalc * Math.cos(alphaCalc) +
              speedCalc * Math.sin(alphaCalc) * speedCalc * Math.sin(alphaCalc)
          ),
      weightCalc
    ),
    accelerationY: updateAcceleration(
      frictionCalc * speedCalc * Math.sin(alphaCalc) +
        9.8 * weightCalc +
        windshieldCalc *
          speedCalc *
          Math.sin(alphaCalc) *
          Math.sqrt(
            speedCalc * Math.cos(alphaCalc) * speedCalc * Math.cos(alphaCalc) +
              speedCalc * Math.sin(alphaCalc) * speedCalc * Math.sin(alphaCalc)
          ),
      weightCalc
    ),
  };
  const accurateObject = {
    height: 0,
    distance: 0,
    speedX: speedCalc * Math.cos(alphaCalc),
    speedY: speedCalc * Math.sin(alphaCalc),
    accelerationX: updateAcceleration(
      -frictionCalc * speedCalc * Math.cos(alphaCalc) -
        windshieldCalc *
          speedCalc *
          Math.cos(alphaCalc) *
          Math.sqrt(
            speedCalc * Math.cos(alphaCalc) * speedCalc * Math.cos(alphaCalc) +
              speedCalc * Math.sin(alphaCalc) * speedCalc * Math.sin(alphaCalc)
          ),
      weightCalc
    ),
    accelerationY: updateAcceleration(
      frictionCalc * speedCalc * Math.sin(alphaCalc) +
        9.8 * weightCalc +
        windshieldCalc *
          speedCalc *
          Math.sin(alphaCalc) *
          Math.sqrt(
            speedCalc * Math.cos(alphaCalc) * speedCalc * Math.cos(alphaCalc) +
              speedCalc * Math.sin(alphaCalc) * speedCalc * Math.sin(alphaCalc)
          ),
      weightCalc
    ),
  };

  object.height += object.speedY / 1000.0;
  object.distance += object.speedX / 1000.0;
  accurateObject.height += accurateObject.speedY / 1000.0;
  accurateObject.distance += accurateObject.speedX / 1000.0;
  let accurateMaxHeight = 0;
  let objectMaxHeight = 0;
  while (object.height > 0) {
    const numberExtraIteration = 10;
    for (let t = 0; t < numberExtraIteration; ++t) {
      accurateObject.speedX =
        accurateObject.speedX +
        accurateObject.accelerationX / (1000.0 * numberExtraIteration);
      accurateObject.speedY =
        accurateObject.speedY -
        accurateObject.accelerationY / (1000.0 * numberExtraIteration);

      accurateObject.accelerationX = updateAcceleration(
        -frictionCalc * accurateObject.speedX -
          windshieldCalc *
            accurateObject.speedX *
            Math.sqrt(
              accurateObject.speedX * accurateObject.speedX +
                accurateObject.speedY * accurateObject.speedY
            ),
        weightCalc
      );
      accurateObject.accelerationY = updateAcceleration(
        frictionCalc * accurateObject.speedY +
          9.8 * weightCalc +
          windshieldCalc *
            accurateObject.speedY *
            Math.sqrt(
              accurateObject.speedX * accurateObject.speedX +
                accurateObject.speedY * accurateObject.speedY
            ),
        weightCalc
      );
      accurateObject.height +=
        accurateObject.speedY / (1000.0 * numberExtraIteration);
      accurateObject.distance +=
        accurateObject.speedX / (1000.0 * numberExtraIteration);
      accurateMaxHeight = Math.max(accurateMaxHeight, accurateObject.height);
    }
    object.speedX = object.speedX + object.accelerationX / 1000.0;
    object.speedY = object.speedY - object.accelerationY / 1000.0;
    object.accelerationX = updateAcceleration(
      -frictionCalc * object.speedX -
        windshieldCalc *
          object.speedX *
          Math.sqrt(
            object.speedX * object.speedX + object.speedY * object.speedY
          ),
      weightCalc
    );
    object.accelerationY = updateAcceleration(
      frictionCalc * object.speedY +
        9.8 * weightCalc +
        windshieldCalc *
          object.speedY *
          Math.sqrt(
            object.speedX * object.speedX + object.speedY * object.speedY
          ),
      weightCalc
    );
    object.height += object.speedY / 1000.0;
    object.distance += object.speedX / 1000.0;
    objectMaxHeight = Math.max(objectMaxHeight, object.height);
  }
  // console.log([
  //   Math.abs(objectMaxHeight - accurateMaxHeight) / objectMaxHeight,
  //   Math.abs(object.distance - accurateObject.distance) / object.distance,
  // ])
  return [
    Math.abs(objectMaxHeight - accurateMaxHeight) / objectMaxHeight,
    Math.abs(object.distance - accurateObject.distance) / object.distance,
  ];
}
function App() {
  const [params, setParams] = useState({
    alpha: 0,
    speed: 0,
    weight: 1,
    friction: 0,
    windshield: 0,
  });
  const [heightError, setHeightError] = useState("");
  const [distanceError, setDistanceError] = useState("");
  const [alpha, setAlpha] = useState("");
  const [speed, setSpeed] = useState("");
  const [weight, setWeight] = useState("");
  const [friction, setFriction] = useState("");
  const [windshield, setWindshield] = useState("");

  const [possibleValuesAlpha, setPossibleValuesAlpha] = React.useState([0, 90]);
  const [possibleValuesSpeed, setPossibleValuesSpeed] = React.useState([
    0,
    kInf,
  ]);
  const [possibleValuesWeight, setPossibleValuesWeight] = React.useState([
    0,
    kInf,
  ]);
  const [possibleValuesFriction, setPossibleValuesFriction] = React.useState([
    0,
    kInf,
  ]);
  const [possibleValuesWindshield, setPossibleValuesWindshield] =
    React.useState([0, kInf]);

  const calculateBorders = (
    secret,
    alphaCalc,
    speedCalc,
    frictionCalc,
    windshieldCalc,
    weightCalc,
    errors
  ) => {
    let left = 0;
    let right = 0;
    switch (secret) {
      case "alpha":
        left = 0;
        right = 90;
        break;
      case "windshield":
        left = 0;
        right = 20;
        break;
      case "friction":
        left = 0;
        right = 20;
        break;
      case "weight":
        left = 0;
        right = kInf;
        break;
      case "speed":
        left = 0;
        right = kInf;
        break;
    }
    let newErrors = [0, 0];
    while (right - left > 0.1) {
      const mid = (left + right) / 2;
      let lower = false;
      switch (secret) {
        case "alpha":
          newErrors = getErrors(
            mid,
            speedCalc,
            frictionCalc,
            windshieldCalc,
            weightCalc
          );
          if (errors.height < newErrors[0] || errors.distance < newErrors[1]) {
            lower = true;
          }
          break;
        case "windshield":
          newErrors = getErrors(
            alphaCalc,
            speedCalc,
            frictionCalc,
            mid,
            weightCalc
          );
          if (errors.height < newErrors[0] || errors.distance < newErrors[1]) {
            lower = true;
          }
          break;
        case "friction":
          newErrors = getErrors(
            alphaCalc,
            speedCalc,
            mid,
            windshieldCalc,
            weightCalc
          );
          if (errors.height < newErrors[0] || errors.distance < newErrors[1]) {
            lower = true;
          }
          break;
        case "weight":
          newErrors = getErrors(
            alphaCalc,
            speedCalc,
            frictionCalc,
            windshieldCalc,
            mid
          );
          if (errors.height < newErrors[0] || errors.distance < newErrors[1]) {
            lower = true;
          }
          break;
        case "speed":
          newErrors = getErrors(
            alphaCalc,
            mid,
            frictionCalc,
            windshieldCalc,
            weightCalc
          );
          if (errors.height < newErrors[0] || errors.distance < newErrors[1]) {
            lower = true;
          }
          break;
      }
      // console.log(errors, newErrors, left, right)
      //lower === true => error is too much
      if (lower) {
        left = mid;
      } else {
        right = mid;
      }
    }
    return [left, kInf];
  };
  const updatePossible = (values) => {
    let lastSecretVariable = "";
    const heightError = decorateValues(values.heightError) / 100;
    const distanceError = decorateValues(values.distanceError) / 100;
    let alphaCalc = 1;
    if (values.alpha != "") {
      alphaCalc = (Math.PI * decorateValues(values.alpha)) / 180;
    } else {
      lastSecretVariable = "alpha";
    }

    let windshieldCalc = 0;
    if (values.windshield != "") {
      windshieldCalc = decorateValues(values.windshield);
    } else {
      lastSecretVariable = "windshield";
    }
    let frictionCalc = 0;
    if (values.friction != "") {
      frictionCalc = decorateValues(values.friction);
    } else {
      lastSecretVariable = "friction";
    }

    let weightCalc = 1;
    if (values.weight != "") {
      weightCalc = decorateValues(values.weight);
    } else {
      lastSecretVariable = "weight";
    }
    let speedCalc = 10;
    if (values.speed != "") {
      speedCalc = decorateValues(values.speed);
    } else {
      lastSecretVariable = "speed";
    }
    if (lastSecretVariable != "") {
      const [left, right] = calculateBorders(
        lastSecretVariable,
        alphaCalc,
        speedCalc,
        frictionCalc,
        windshieldCalc,
        weightCalc,
        {
          height: heightError,
          distance: distanceError,
        }
      );
      switch (lastSecretVariable) {
        case "alpha":
          setPossibleValuesAlpha([left, right]);
          break;
        case "speed":
          setPossibleValuesSpeed([left, right]);
          break;
        case "weight":
          setPossibleValuesWeight([left, right]);
          break;
        case "friction":
          setPossibleValuesFriction([left, right]);
          break;
        case "windshield":
          setPossibleValuesWindshield([left, right]);
          break;
      }
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setParams({
      heightError: decorateValues(heightError),
      distanceError: decorateValues(distanceError),
      alpha: (Math.PI * decorateValues(alpha)) / 180,
      speed: decorateValues(speed),
      weight: decorateValues(weight),
      friction: decorateValues(friction),
      windshield: decorateValues(windshield),
    });
  };
  return (
    <section className={styles.body}>
      <Graph params={params} />
      <div className={styles.form}>
        <form onSubmit={handleSubmit} className={styles.data}>
          <input
            className={styles.textarea}
            placeholder="Погрешность дальность в %"
            value={distanceError}
            onChange={(e) => {
              setDistanceError(e.target.value);
              if (heightError != "" && e.target.value != "") {
                updatePossible({
                  alpha: alpha,
                  speed: speed,
                  weight: weight,
                  friction: friction,
                  windshield: windshield,
                  heightError: heightError,
                  distanceError: e.target.value,
                });
              }
            }}
            required
          ></input>
          <input
            className={styles.textarea}
            placeholder="Погрешность высоты в %"
            value={heightError}
            onChange={(e) => {
              setHeightError(e.target.value);
              if (e.target.value != "" && distanceError != "") {
                updatePossible({
                  alpha: alpha,
                  speed: speed,
                  weight: weight,
                  friction: friction,
                  windshield: windshield,
                  heightError: e.target.value,
                  distanceError: distanceError,
                });
              }
            }}
            required
          ></input>
          <div className={styles.parameter}>
            <p className={styles.info}>
              Доступные значения угла градуса:[
              {outputDecorate(possibleValuesAlpha[0]) +
                ", " +
                outputDecorate(possibleValuesAlpha[1])}
              ]
            </p>
            <input
              className={styles.textarea}
              placeholder="Угол наклона градусах"
              value={alpha}
              onChange={(e) => {
                setAlpha(e.target.value);
                if (heightError != "" && distanceError != "") {
                  updatePossible({
                    alpha: e.target.value,
                    speed: speed,
                    weight: weight,
                    friction: friction,
                    windshield: windshield,
                    heightError: heightError,
                    distanceError: distanceError,
                  });
                }
              }}
              required
            ></input>
          </div>
          <div className={styles.parameter}>
            <p className={styles.info}>
              Доступные значения скорости м/с:[
              {outputDecorate(possibleValuesSpeed[0]) +
                ", " +
                outputDecorate(possibleValuesSpeed[1])}
              ]
            </p>
            <input
              className={styles.textarea}
              placeholder="Стартовая скорость м/с"
              value={speed}
              onChange={(e) => {
                setSpeed(e.target.value);
              }}
              required={true}
            ></input>
          </div>
          <div className={styles.parameter}>
            <p className={styles.info}>
              Доступные значения масса кг:[
              {outputDecorate(possibleValuesWeight[0]) +
                ", " +
                outputDecorate(possibleValuesWeight[1])}
              ]
            </p>
            <input
              className={styles.textarea}
              placeholder="Масса кг"
              value={weight}
              onChange={(e) => {
                setWeight(e.target.value);
                if (heightError != "" && distanceError != "") {
                  updatePossible({
                    alpha: alpha,
                    speed: speed,
                    weight: e.target.value,
                    friction: friction,
                    windshield: windshield,
                    heightError: heightError,
                    distanceError: distanceError,
                  });
                }
              }}
              required
            ></input>
          </div>
          <div className={styles.parameter}>
            <p className={styles.info}>
              Доступные значения Вязкость м^2/c:[
              {outputDecorate(possibleValuesFriction[0]) +
                ", " +
                outputDecorate(possibleValuesFriction[1])}
              ]
            </p>
            <input
              className={styles.textarea}
              placeholder="Вязкость м^2/c"
              value={friction}
              onChange={(e) => {
                setFriction(e.target.value);
                if (heightError != "" && distanceError != "") {
                  updatePossible({
                    alpha: alpha,
                    speed: speed,
                    weight: weight,
                    friction: e.target.value,
                    windshield: windshield,
                    heightError:  heightError,
                    distanceError: distanceError,
                  });
                }
              }}
            ></input>
          </div>
          <div className={styles.parameter}>
            <p className={styles.info}>
              Доступные значения аэродин. сопр.:[
              {outputDecorate(possibleValuesWindshield[0]) +
                ", " +
                outputDecorate(possibleValuesWindshield[1])}
              ]
            </p>
            <input
              className={styles.textarea}
              placeholder="Коэффициент аэродин. сопр."
              value={windshield}
              onChange={(e) => {
                setWindshield(e.target.value);
                if (heightError != "" && distanceError != "") {
                  updatePossible({
                    alpha: alpha,
                    speed: speed,
                    weight: weight,
                    friction: friction,
                    windshield: e.target.value,
                    heightError: heightError,
                    distanceError: distanceError,
                  });
                }
              }}
            ></input>
          </div>
          <input
            className={styles.submit}
            type="submit"
            value="Промоделировать"
          ></input>
        </form>
      </div>
    </section>
  );
}

export default App;
